package com.samip.api_facth;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    Context context;
    List<Post> postList;

    public RecyclerAdapter(Context context, List<Post> postList) {
        this.context = context;
        this.postList = postList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Post post = postList.get(position);
        holder.id_txt.setText("ID : " + post.getId());
        holder.uid_txt.setText("UserId : " +post.getUserId());
        holder.text_txt.setText("Text : " + post.getText());
        holder.title_txt.setText("Title : " + post.getTitle());
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView id_txt,uid_txt,title_txt,text_txt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id_txt = (TextView) itemView.findViewById(R.id.id_txt);
            uid_txt = (TextView) itemView.findViewById(R.id.uid_txt);
            title_txt = (TextView) itemView.findViewById(R.id.title_txt);
            text_txt = (TextView) itemView.findViewById(R.id.text_txt);
        }
    }
}
